#include <avr/sleep.h>
#include <SD.h>
#include <Wire.h>
#include <SPI.h>
#include <XBee.h>
#include <TapirUtils.h>
#include <TapirGlobal.h>
#include <DS3231.h>
#include "c329.h"

#define SEND_NODE_INFO 0x28
#define SEND_NODE_IMAGES 0x30
#define DEBUG_LED 35

const static int INT0_PIR = 0;   // pin 2 on the board
const static int INT4_RTC = 4;   // pin 19 on the board
enum IRQ_tapir {IRQ_RTC, IRQ_PIR};
volatile IRQ_tapir IRQ_flag;     // specifies which ISR occurred
DS3231 RTC;
static DateTime interruptTime;
static char *IMAGES_DIR = "/";
static uint8_t START_IMAGE_TX = 0x19;
static uint8_t END_IMAGE_TX = 0x20;
XBee xbee;
TapirCommand tr_command;

void ISR_rtc() {
    IRQ_flag = IRQ_RTC;
}

void ISR_pir() {
    IRQ_flag = IRQ_PIR;
}

void setup() {
    Serial.begin(115200);
    pinMode(2, INPUT);           // PIR interrupt
    pinMode(19, INPUT);          // RTC interrupt
    pinMode(DEBUG_LED, OUTPUT);  // Board LED
    pinMode(10, OUTPUT);         // For SPI Communication
    digitalWrite(19, HIGH);
    Wire.begin();
    RTC.begin();
    C329::begin();
    SPI.begin();
    Serial.println("Initializing SD Card");
    delay(2000);
    if (!SD.begin(4)) {
        Serial.println("SD initialization failed");
        return;
    }
    Serial.println("SD initialized");

    DateTime start = RTC.now();
    RTC.clearINTStatus();
    // RTC.enableInterrupts(6, 0, 0);
    RTC.enableInterrupts(EveryHour);
    attachInterrupt(INT4_RTC, ISR_rtc, LOW);
    blink(DEBUG_LED, 20, 1000);
}

void loop() {
    Serial.print("Entering Power Down Mode:");
    C329::powerdown();
    delay(100);
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    attachInterrupt(INT0_PIR, ISR_pir, LOW);
    sleep_mode();
    sleep_disable();
    detachInterrupt(INT0_PIR);
    detachInterrupt(INT4_RTC);
    // Wait for IRQ.
    // Interrupt should leave a mark to identify
    // if was because of movement detection or
    // the RTC.
    switch (IRQ_flag) {
    case IRQ_RTC :
        handle_rtc_interrupt();
        break;
    case IRQ_PIR :
        handle_movement_detection();
        break;
    default :
        break;
    }
}

void handle_movement_detection() {
    File CamPicture;
    char filename[13];
    long time = 0;
    char errmsg[50];
    int light;
    int voltage;

    digitalWrite(DEBUG_LED, LOW);
    C329::powerup();
    time = millis();
    if (!C329::init(0x00, errmsg)) {
        if (errmsg != NULL) {
            Serial.println(errmsg);
        }
    }
    else {
        for(uint8_t tries = 0; tries < 3; tries++){
            snprintf(filename, sizeof(filename), "IMAGE000.JPG");
            for (int i = 0; i < 1000; i++) {
                filename[5] = '0' + i/100;
                uint8_t j = i%100;
                filename[6] = '0' + j/10;
                filename[7] = '0' + j%10;
                // create if does not exist, do not open existing, write, sync
                // after write
                if (! SD.exists(filename)) {
                    break;
                }
            }
            CamPicture = SD.open(filename, FILE_WRITE);

            light = analogRead(LIGHT_SENSOR);
            if (light < LIGHT_THRESHOLD) {
                digitalWrite(CAMERA_FLASH, HIGH);
            }

            if (!C329::snapshot(&CamPicture, true, errmsg)) {
                if (errmsg != NULL) {
                    Serial.println(errmsg);
                    break;
                }
            }
            CamPicture.close();
        }
    }
    C329::powerdown();
    time = millis() - time;
    Serial.print("Elapsed Time: ");
    Serial.println(time);

#ifdef LOG_LIGHT
    // analogReference(INTERNAL2V56);
    // voltage = analogRead(A0);
    DateTime now = RTC.now();
    lightlog = SD.open("Log_Data.csv", FILE_WRITE);
    lightlog.print(now.year(), DEC);
    lightlog.print('/');
    lightlog.print(now.month(), DEC);
    lightlog.print('/');
    lightlog.print(now.date(), DEC);
    lightlog.print(',');
    lightlog.print(now.hour(), DEC);
    lightlog.print(':');
    lightlog.print(now.minute(), DEC);
    lightlog.print(':');
    lightlog.print(now.second(), DEC);
    lightlog.print(',');
    lightlog.println(light);
    // lightlog.print(',');
    // lightlog.println(voltage);
    lightlog.close();
    // analogReference(DEFAULT);
#endif LOG_LIGHT
    digitalWrite(DEBUG_LED, HIGH);
}

void handle_rtc_interrupt() {
    boolean timeout = true;
    // wake-up radio
    long deadline = millis() + 10000;
    while (deadline < millis()) {
	if (xbee.getResponse().isAvailable()) {
	    timeout = false;
	    break;
	}
	continue;
    }
    if (! timeout) {
        receive_command(&tr_command);
        if (tr_command.id == SEND_NODE_INFO) {
            if (tr_command.remoteAddr.getLsb() != NULL) {
                send_node_info(tr_command.remoteAddr);
            }
        }
        tr_command.id = NULL;
        tr_command.remoteAddr.setLsb(NULL);
        tr_command.remoteAddr.setMsb(NULL);
	timeout = true;
        deadline = millis() + 10000;
        while (deadline < millis()) {
            if (xbee.getResponse().isAvailable()) {
                timeout = false;
                break;
            }
            continue;
        }
        if (! timeout) {
            receive_command(&tr_command);
            if (tr_command.id == SEND_NODE_IMAGES) {
                if (tr_command.remoteAddr.getLsb() != NULL) {
                    send_node_images(tr_command.remoteAddr);
                }
            }
        }
    }

    RTC.clearINTStatus();
    attachInterrupt(INT4_RTC, ISR_rtc, LOW);

    // sleep radio again
}

static void receive_command(TapirCommand *command) {
  ZBRxResponse rx = ZBRxResponse();
  xbee.readPacket(1000);
  if (xbee.getResponse().isAvailable()) {
#ifdef DEBUG
      Serial.println("packet received");
#endif
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      xbee.getResponse().getZBRxResponse(rx);
      if (rx.getDataLength() > 0) {
        command->id = rx.getData(0);
        command->remoteAddr = rx.getRemoteAddress64();
      }
    }
    else {
      // ignore other API Ids
    }
  }
}

// Return the number of JPEG images in SD Card. It is assumed no more than 256 images are being stored in SD.
static uint8_t count_images() {
  uint8_t count = 0;
  File dir = SD.open(IMAGES_DIR);
  File entry;
  for (;;) {
    // TODO: alternative, more efficient approach to count files without opening each file.
    entry = dir.openNextFile();
    if (! entry) {
      break;
    }
    if (! entry.isDirectory()) {
      count++;
      entry.close();
    }
  }
  return count;
}

static void send_node_info(XBeeAddress64 sink_node) {
  ZBTxRequest zbTx;
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();
#ifdef DEBUG
  Serial.print("Got a request to send info from this node: ");
  Serial.print(sink_node.getMsb(), HEX);
  Serial.println(sink_node.getLsb(), HEX);
#endif
  // send # images of the node
  // send other diagnostic data
  // TODO: replace hard-coded data with number of images in SD Card
  uint8_t number_images = count_images();
  uint8_t payload[] = {number_images};
  zbTx = ZBTxRequest(sink_node, payload, sizeof(payload));
  xbee.send(zbTx);
  // clear xbee's _receive buffer_
  xbee.readPacket(1000);
  xbee.getResponse();
}

static void send_node_images(XBeeAddress64 sink_node) {
  File dir = SD.open(IMAGES_DIR);
  File entry;
#ifdef DEBUG
  Serial.println("Got a request to send images");
#endif
  for (;;) {
    entry = dir.openNextFile();
    if (! entry) {
      break;
    }
    send_image(sink_node, &entry);
    // close image file
    entry.close();
  }
}

static void send_image(XBeeAddress64 sink_node, File *file) {
  image_info info;
  image_block block;
  uint8_t bytes_read;
  uint8_t buf[MAX_BYTES_PER_BLOCK];
  uint32_t file_size = file->size();
  info.size = file_size;
#ifdef DEBUG
  Serial.print("Sending file: ");
  Serial.print(file->name());
  Serial.print(" Size: ");
  Serial.println(file_size);
#endif
  uint32_t fragments = file_size % (uint32_t) MAX_BYTES_PER_BLOCK == 0 ?
    file_size / (uint32_t) MAX_BYTES_PER_BLOCK :
    (file_size / (uint32_t) MAX_BYTES_PER_BLOCK) + 1;
  info.blocks = fragments;
  // FIXME: need to send actual time. Filesystem should give this info, right?! °^°
  info.creation_time = 1402011883;

  // start with sending image info
  // send actual image in blocks
  uint16_t seq = 1;
  while (file->available() > 0) {
    bytes_read = file->read(buf, MAX_BYTES_PER_BLOCK - 5);
    if (bytes_read > 0) {
      block.sequence = seq;
      block.data_len = bytes_read;
      block.data = buf;
      send_block(&sink_node, &block);
      seq++;
    }
  }
#ifdef DEBUG
  Serial.println("");
#endif
}

void send_block(XBeeAddress64 *sink_node, image_block *block) {
  uint8_t buf[DIGI_PAYLOAD_MAX_BYTES];
  ZBTxRequest zbTx;
  buf[0] = (block->sequence >> 8) & 0xff;
  buf[1] = (block->sequence) & 0xff;
  buf[2] = (block->data_len);
  // copy actual image data to the 4th position of this local buffer
  memcpy(buf + 3, block->data, block->data_len);
  // total length is data lenght plus 3 bytes from block's header size
  zbTx = ZBTxRequest(*sink_node, buf, block->data_len + 3);
  // optionally we could set frame id to 0 so we don't have to read back the Tx Status frame
  // zbTx.setFrameId(0);
  xbee.send(zbTx);
  // read Tx Status Frame
  xbee.readPacket(10);
  // Since we send image data in a best-effort manner don't care _tx status_
  // if (xbee.getResponse().isAvailable()) {
  //   if (xbee.getResponse().getApiId() == TX_STATUS_RESPONSE) {
  //     //
  //   }
  // }
  // we already do some _wasting_ time between each block send so this might no
  // be needed
  delay(60);
  Serial.print(".");
}

// ##
void blink(int pin_led, int times, int wait) {
    for (int i=0; i < times; i++) {
        digitalWrite(pin_led, HIGH);
        delay(wait);
        digitalWrite(pin_led, LOW);
        if (i + 1 < times) {
            delay(wait);
        }
    }
}
