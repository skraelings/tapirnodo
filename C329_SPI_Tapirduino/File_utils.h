/*-------------------------------------
Author: Joel Palomino
Date Created: 23/02/2015
Date Modified: 24/02/2015
Description: These utilities are used to created a File with all the imgaes
            taken in the node. All the images have and index, the timestamp
            and the filename.
-------------------------------------*/

#include "Arduino.h"
#include <SD.h>
#include <DS3231.h>

#define Metadata "/CAM/Filedat.txt"
#define FileName "/CAM/IMAGE000.jpg"

struct ImageInfo{uint32_t id; uint32_t timestamp; char filepath[20]; };

class TapirFile{
  public:
    void WriteMetaData(uint32_t Index[], uint32_t Count, DS3231 *Clock);
    boolean GetIndex(ImageInfo *Matrix,uint32_t Index);
    boolean MarkIndex(uint32_t Index);
  private:
    void WriteIndex(char buf[], DS3231 *Clock);
    uint32_t NextIndex();
    uint32_t GetNumber(File myFile);
    boolean FindIndex(File myFile, uint32_t *Number);
    void ReadData(File myFile, ImageInfo *Matrix, uint32_t Number);
};
