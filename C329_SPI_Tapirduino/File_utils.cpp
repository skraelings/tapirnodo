#include "File_utils.h"
#include <SD.h>
#include <SPI.h>

/*---------Write MetaData--------
Function to write the Metadata of
the pictures
------------------------------*/
void TapirFile::WriteMetaData(uint32_t Index[], uint32_t Count, DS3231 *Clock){
  char Name[20];
  uint8_t j,i;
  
  strcpy(Name, FileName);
  for(i=0; i<Count; i++){
    Name[10] = '0' + Index[i]/100;
    j = Index[i]%100;
    Name[11] = '0' + j/10;
    Name[12] = '0' + j%10;
    WriteIndex(Name, Clock);
  }
}

/*--------Get Index------------
  This function gets the information of the files
  that have not been transmited
----------------------------*/
boolean TapirFile::GetIndex(ImageInfo *Matrix, uint32_t Index){
  File IndexLog;
  uint32_t Number=0;
  boolean Skip;
  
  IndexLog = SD.open(Metadata,FILE_WRITE);
  IndexLog.seek(0);
  while( IndexLog.available() ){
    Skip = FindIndex(IndexLog, &Number);
    if( Number != Index ){
      while( (IndexLog.read()!=0x0A) && (IndexLog.available()) );
    }
    else{
      if(Skip)
        Number = IndexLog.read();
      ReadData(IndexLog, Matrix, Index);
      IndexLog.close();
      return true;
    }
  }
  IndexLog.close();
  return false;
}

/*----------Mark Index-----------
  Function to Mark the index with
  an # to indicated Image transmited
-------------------------------*/
boolean TapirFile::MarkIndex(uint32_t Index){
  File IndexLog;
  uint32_t Number;
  boolean Mark;
  unsigned long Current;
  
  IndexLog = SD.open(Metadata,FILE_WRITE);
  IndexLog.seek(0);
  while( IndexLog.available() ){
    Current = IndexLog.position();
    Mark = FindIndex(IndexLog, &Number);
    if( Number != Index){
      while( (IndexLog.read()!=0x0A) && (IndexLog.available()) );
    }
    else{
      if(Mark){
        IndexLog.seek(Current);
        IndexLog.print("#");
        IndexLog.print(Index);
        IndexLog.close();
        return true;
      }
      IndexLog.close();
      return false;
    }
  }
  IndexLog.close();
  return false;
}

/*--------WriteIndex------------
  This function writes in the log the 
  information of the pitcure taken
-------------------------------*/
void TapirFile::WriteIndex(char buf[], DS3231 *Clock){
  uint32_t count;
  DateTime hora;
  long tiempo;
  File IndexLog;
  
  count = NextIndex() + 1;
  hora = Clock->now();
  IndexLog = SD.open(Metadata,FILE_WRITE);
  delay(10);
  IndexLog.print(count);
  IndexLog.print("  ");
  tiempo = hora.get() + 946684800;
  IndexLog.print(tiempo);
  IndexLog.print(" ");
  IndexLog.println(buf);
  IndexLog.close();
  IndexLog = SD.open("/");
  delay(10);
}

/*---------Next Index----------
  This function searches for the next
  index in the file
-----------------------------*/
uint32_t TapirFile::NextIndex(){
  File IndexLog;
  uint32_t Index=0;

  if(SD.exists(Metadata)){
    IndexLog = SD.open(Metadata,FILE_READ);
    delay(10);
    while(IndexLog.available()){
      Index=0;
      Index = GetNumber(IndexLog);
      while((IndexLog.read() != 0x0A) && (IndexLog.available()));
    }
    IndexLog.close();
    return Index;
  }
  else
    return -1;
}

/*---------Get Number--------
  This function gets the index
  of the last member in the file
---------------------------*/
uint32_t TapirFile::GetNumber(File myFile){
  uint32_t Index=0, Num;

  while(true){
    Num = myFile.read();
    if(Num == '#'){
      Index=0;
    }
    else if(Num == ' '){
      return Index;
    }
    else{
      Index = Index*10;
      Index += (Num-'0');
    }
  }
}

/*---------Find Index---------
  This function reads if there are
  no transmited index
----------------------------*/
boolean TapirFile::FindIndex(File myFile, uint32_t *Number){
  uint32_t Index=0,Num;
  boolean Found = true;

  while(true){
    Num = myFile.read();
    if(Num == '#'){
      Found = false;
    }
    else if(Num == ' '){
      *Number = Index;
      return Found;
    }
    else{
      Index = Index*10;
      Index += (Num-'0');
    }
  }
}

/*---------Read Data-------
  This file reads the data in the file
  and writes it into a structure
--------------------------*/
void TapirFile::ReadData(File myFile, ImageInfo *Matrix, uint32_t Number){
  uint32_t Index;
  bool search=true;
  char letter;
  
  Matrix->id = Number;
  Index = GetNumber(myFile);
  Matrix->timestamp = Index;
  Index=0;
  while(search){
    letter = myFile.read();
    if(letter == 0x0D){
      search = false;
    }
    else{
      Matrix->filepath[Index] = letter;
      Index++;
    }
  }
}
