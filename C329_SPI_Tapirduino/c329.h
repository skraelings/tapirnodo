#ifndef C329_H
#define C329_H

#define LIGHT_SENSOR A1
#define CAMERA_FLASH 26
#define LIGHT_THRESHOLD 80

class C329 {
 public:

    static void begin();
    static boolean init(uint8_t quality, char *errmsg);
    static void powerup();
    static void powerdown();
    static boolean snapshot(File *savefile, boolean ir_enabled, char *errmsg);

 private:

    static uint8_t inputCommand[8];
    static uint8_t outputCommand[8];
    static const uint8_t HOLD_PIN	= 22;
    static const uint8_t CAMERA_CS	= 23;
    static const uint8_t POWER_CONTROL	= 24;
    static const uint8_t SPI_CONTROL	= 25;

    static boolean SPISendSyncCmd();
    static boolean SPISendInitCmd();
    static boolean SPISendQualityCmd(uint8_t Quality);
    static boolean SPITakeSnapshot();
    static boolean SPIReadPicture(File *savefile);
    static void SendCommand();
    static void ReadCommand(uint8_t buf[], uint16_t buf_size);
    static inline void SetCommand(uint8_t Param1,uint8_t Param2, uint8_t Param3, uint8_t Param4, uint8_t Param5);
};

#endif
