#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <DS3231.h>
#include <avr/sleep.h>
#include "C329.h"
#include "File_utils.h"

#define LOG_LIGHT
char errmsg[50];
static char filename[18];
File CamPicture, lightlog;
DS3231 Hour;
TapirFile MetaData;

void setup(){
  uint8_t state=0;
  
  pinMode(2,INPUT);  //Sensor PIR
  pinMode(27,OUTPUT);
  pinMode(28,OUTPUT);
  pinMode(53,OUTPUT);  //SPI Communication
  pinMode(35,OUTPUT);  //Board LED
  digitalWrite(27,HIGH);
  digitalWrite(28,HIGH);
  Serial.begin(115200);
  
  if(!SD.begin(4))
  {
    Serial.println("SD Initialization Failed");
    return;
  }
  Serial.println("SD Initialized");
  
  C329::begin();
  C329::powerdown();
  
  Wire.begin();
  Hour.begin();
  
  for(uint8_t i=0;i<20;i++)
  {
    if(state==0)
    {
      digitalWrite(35,LOW);
      state=1;
    }
    else
    {
      digitalWrite(35,HIGH);
      state=0;
    }
    delay(1000);
  }
  digitalWrite(35,HIGH);
  strcpy(filename, "/CAM/IMAGE000.JPG");
}

void loop(){
  uint8_t i=0,j,tries;
  int light;
  static uint32_t ind[8], count=0;
  float voltage;
  boolean success;
  
  Serial.println("Entering Power Down Mode:");
  delay(100);
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  attachInterrupt(0,Pin_Change,LOW);
  sleep_cpu();
  sleep_disable();
  detachInterrupt(0);
  
  digitalWrite(35,LOW);
  analogReference(INTERNAL2V56);
  C329::powerup();
  voltage = analogRead(A0);
  voltage = 2*2.56*voltage/1024;
  analogReference(DEFAULT);
  success = true;
  
  if( !C329::init(0x00,errmsg) ){
    Serial.println(errmsg);
    success = false;
  }
  else{
    Serial.println("Initialize success");
    for(tries=0; tries<3;tries++){
      //strcpy(filename, "/CAM/IMAGE000.JPG");
      while( SD.exists(filename) ) {
        i = (filename[10]-'0')*100 + (filename[11]-'0')*10 + (filename[12]-'0');
        i++;
        filename[10] = '0' + i/100;
        j=i%100;
        filename[11] = '0' + j/10;
        filename[12] = '0' + j%10;
      }
      CamPicture = SD.open(filename, FILE_WRITE);
      delay(10);
      
      light = analogRead(LIGHT_SENSOR);
      if (light < LIGHT_THRESHOLD) {
	digitalWrite(CAMERA_FLASH, HIGH);
      }
      
      if (!C329::snapshot(&CamPicture, true, errmsg)){
        Serial.println(errmsg);
        success = false;
      }
      CamPicture.close();
      ind[count]=i;
      count++;
      Serial.println("Picture taken");
    }
  }

  C329::powerdown();
  digitalWrite(35,HIGH);
  
  if(count!=0){
    MetaData.WriteMetaData(ind, count, &Hour);
    count=0;
  }
  
  if(success){
    while(!digitalRead(2));
    success = true;
  }
  
  #ifdef LOG_LIGHT
  DateTime now = Hour.now();
  delay(10);
  lightlog = SD.open("/CAM/Log_Data.csv", FILE_WRITE);
  lightlog.print(now.year(), DEC);
  lightlog.print('/');
  lightlog.print(now.month(), DEC);
  lightlog.print('/');
  lightlog.print(now.date(), DEC);
  lightlog.print(',');
  lightlog.print(now.hour(), DEC);
  lightlog.print(':');
  lightlog.print(now.minute(), DEC);
  lightlog.print(':');
  lightlog.print(now.second(), DEC);
  lightlog.print(',');
  lightlog.print(light);
  lightlog.print(',');
  if(!success){
    lightlog.print(errmsg);
    lightlog.print(',');
  }
  lightlog.println(voltage);
  lightlog.close();
  #endif LOG_LIGHT
}

void Pin_Change()
{
  delay(10);
}
