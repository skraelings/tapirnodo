/* ---------------------------------------------------------- */
/* SPI Camera Library for Arduino (for Tapirduino) */
/* Author: Joel Palomino */
/* Date: 18-09-2014 */
/* Modification: 18-09-2014 */
/* Observation: The SPI Camera speed is decided by the Master */
/*            In this case the camera speed is define by the SD card */
/*            which is FCPU/2 */
/* ---------------------------------------------------------- */

#include <SPI.h>
#include <SD.h>
#include "c329.h"

uint8_t C329::inputCommand[8];
uint8_t C329::outputCommand[8];

void C329::SendCommand() {
    uint8_t i;
    digitalWrite(CAMERA_CS, LOW);
    for(i = 0; i < 8; i++)
        SPI.transfer(inputCommand[i]);
    digitalWrite(CAMERA_CS, HIGH);
}

void C329::ReadCommand(uint8_t buf[], uint16_t buf_size) {
  for(uint16_t i = 0; i < buf_size; i++) {
      digitalWrite(CAMERA_CS, LOW);
      buf[i] = SPI.transfer(0x00);
      digitalWrite(CAMERA_CS, HIGH);
  }
}

inline
void C329::SetCommand(uint8_t Param1, uint8_t Param2, uint8_t Param3,
                      uint8_t Param4, uint8_t Param5) {
    inputCommand[0] = 0xFF;
    inputCommand[1] = 0xFF;
    inputCommand[2] = 0xFF;
    inputCommand[3] = Param1;
    inputCommand[4] = Param2;
    inputCommand[5] = Param3;
    inputCommand[6] = Param4;
    inputCommand[7] = Param5;
}

boolean C329::SPISendSyncCmd() {
    uint8_t time_out = 0;
    // Wait for Device to be ready
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }
    // Set SYNC Command
    SetCommand(0x0D, 0x00, 0x00, 0x00, 0x00);
    SendCommand();

    // Wait for Device to be ready
    time_out = 0;
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }
    ReadCommand(outputCommand, 8);
    if((outputCommand[3] == 0x0E) && (outputCommand[4] == 0x0D)) {
        ReadCommand(outputCommand, 8);
        if(outputCommand[3] == 0x0D) {
            // After recieving the sync command from the camera send a sync
            // command to the camera
            SetCommand(0x0E, 0x00, 0x00, 0x00, 0x00);
            SendCommand();
            return true;
        }
        return false;
    }
    return false;
}

boolean C329::SPISendInitCmd() {
    uint8_t time_out = 0;
    // Wait for Device to be ready
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }

    // Set Init Command
    SetCommand(0x01, 0x00, 0x87, 0x01, 0x07);
    SendCommand();

    // Wait for Device to be ready
    time_out = 0;
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }
    ReadCommand(outputCommand, 8);
    return ((outputCommand[3] == 0x0E) && (outputCommand[4] == 0x01));
}

boolean C329::SPISendQualityCmd(uint8_t Quality) {
    uint8_t time_out = 0;
    // Wait for Device to be ready
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }

    // Set Quality Command
    SetCommand(0x10, Quality, 0x00, 0x00, 0x00);
    SendCommand();
    delay(1);

    // Wait for Device to be ready
    time_out = 0;
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }
    ReadCommand(outputCommand, 8);
    return ((outputCommand[3] == 0x0E) && (outputCommand[4] == 0x10));
}

boolean C329::SPITakeSnapshot() {
    uint8_t time_out = 0;
    // Wait for Device to be ready
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }

    // Set Snapshot Command
    SetCommand(0x05, 0x00, 0x00, 0x00, 0x00);
    SendCommand();
    delay(1);

    // Wait for Device to be ready
    time_out = 0;
    while((digitalRead(HOLD_PIN) == HIGH) && (time_out < 50)) {
        time_out++;
        return false;
    }
    ReadCommand(outputCommand, 8);
    return ((outputCommand[3] == 0x0E) && (outputCommand[4] == 0x05));
}

boolean C329::SPIReadPicture(File *savefile) {
    uint32_t picture_size = 0;
    uint16_t data_size = 0;
    uint8_t picture_buf[64];

    // Wait for Device to be ready
    while(digitalRead(HOLD_PIN) == HIGH)
        continue;

    // Send ReadCommand
    SetCommand(0x04, 0x05, 0x00, 0x00, 0x00);
    SendCommand();
    delay(1);

    // Wait for Device to be ready
    while(digitalRead(HOLD_PIN) == HIGH)
        continue;
    ReadCommand(outputCommand, 8);
    if((outputCommand[3] == 0x0E) && (outputCommand[4] == 0x04)) {
        // Wait for Device to be ready
        while(digitalRead(HOLD_PIN) == HIGH)
            continue;
        ReadCommand(outputCommand, 8);
        if(outputCommand[3] == 0x0A) {
            picture_size = uint32_t(outputCommand[5])
                + uint32_t(outputCommand[6]*0x100)
                + uint32_t(outputCommand[7]*0x10000);
            // Wait for Device to be ready
            while(digitalRead(HOLD_PIN) == HIGH)
                continue;
            while(picture_size > 0) {
                data_size = min(picture_size, 64);
                ReadCommand(picture_buf, data_size);
                savefile->write(picture_buf, data_size);
                picture_size -= data_size;
            }
            return true;
        }
    }
    return false;
}

void C329::begin() {
    // TODO(Reynaldo): decouple from light sensor
    pinMode(LIGHT_SENSOR, INPUT);    // Camera Light Sensor
    pinMode(HOLD_PIN, INPUT);        // Device HOLD Pin
    pinMode(CAMERA_CS, OUTPUT);      // Camera CS
    pinMode(POWER_CONTROL, OUTPUT);  // Device Power Control
    pinMode(SPI_CONTROL, OUTPUT);    // Camera SPI Control
    pinMode(CAMERA_FLASH, OUTPUT);   // Camera Flash
    digitalWrite(CAMERA_CS, HIGH);
    digitalWrite(CAMERA_FLASH, LOW);
}

void C329::powerup() {
    digitalWrite(POWER_CONTROL, HIGH);
    digitalWrite(SPI_CONTROL, LOW);
    // burn cycles this amount of time for camera to be ready
    delay(800);
}

void C329::powerdown() {
    digitalWrite(POWER_CONTROL, LOW);
    digitalWrite(SPI_CONTROL, HIGH);
}

boolean C329::init(uint8_t quality, char *errmsg) {
    if (SPISendSyncCmd()) {
        if (SPISendInitCmd()) {
            delay(1);
            if (SPISendQualityCmd(quality)) {
                return true;
            } else {
                snprintf(errmsg, sizeof(errmsg), "Failed at setting quality");
            }
        } else {
            snprintf(errmsg, sizeof(errmsg), "Init failed");
        }
    } else {
        snprintf(errmsg, sizeof(errmsg), "Failed to sync");
    }
    return false;
}

boolean C329::snapshot(File *savefile, boolean ir_enabled, char *errmsg) {
    if (SPITakeSnapshot()) {
        if (SPIReadPicture(savefile)) {
            if (ir_enabled) {
                digitalWrite(CAMERA_FLASH, LOW);
            }
            return true;
        } else {
            snprintf(errmsg, sizeof(errmsg), "Failed to read picture");
        }
    } else {
        snprintf(errmsg, sizeof(errmsg), "Snapshot command failed");
    }
    return false;
}
